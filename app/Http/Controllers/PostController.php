<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{

    public function store(PostRequest $postRequest)
    {

    }
    public function getDashboard()
    {
        $posts=Post::orderBy('created_at', 'desc')->get();
        return view('dashboard', ['posts'=>$posts]);
    }
    public function postCreatePost(PostRequest $request)
    {
        $post = new Post();
        $post->body = $request['body'];
        if($request->user()->posts()->save($post))
        {
            $massage='Post Successfully Created....!';
        }else{
            $massage='There was an error....!';
        }
        return redirect()->route('dashboard')->with(['massage'=>$massage]);
    }

    public function getDeletePost($post_id){
        $post=Post::where('id', $post_id)->first();

        $post->Delete();
        return redirect()->route('dashboard')->with(['massage'=>'Successfully Delete']);
    }
}