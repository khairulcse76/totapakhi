<?php

namespace App\Http\Controllers;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserSingInRequest;
use App\User;
use Illuminate\Auth\Events\Attempting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function store(UserRequest $userRequest)
    {

    }
    public function postSignUp(UserRequest $request){

        $name=$request['name'];
        $email=$request['email'];
        $password=bcrypt($request['password']);

        $user = new User();

        $user->name = $name;
        $user->email = $email;
        $user->password = $password;

        $user->save();

        Auth::login($user);
       return redirect()->route('dashboard');
    }

    public function postSignIn(UserSingInRequest $request){

        if(Auth::attempt(['email'=>$request['email'], 'password'=>$request['password']])){

            return redirect()->route('dashboard');
        }
        return redirect()->back();
    }
}
