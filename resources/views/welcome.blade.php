   @extends('layouts.master')
   @section('title')
       Totapakhi
   @endsection

   @section('content')
    <div class="row">
        <div class="col-md-6">
            <p class="h2 text-success">Sing Up</p>
            <form action="{{ route('signup') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Your Full Name</label>
                    <input class="form-control" type="text" name="name" id="name" value="{{Request::old('name')}}">
                    @if ($errors->has('name'))
                        <span style="color: red">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif

                </div>
                <div class="form-group">
                    <label for="email">Your Email</label>
                    <input class="form-control" type="text" name="email" id="email" value="{{Request::old('email')}}">
                    @if ($errors->has('email'))
                        <span style="color: red">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="password">Your Password</label>
                    <input class="form-control" type="password" name="password" id="password" value="{{Request::old('password')}}">
                    @if ($errors->has('password'))
                        <span style="color: red">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <button class="btn-primary btn" type="submit">Submit</button>
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>
        </div>

        <div class="col-md-6">
            <p class="h2 text-success">Signin</p>
            <form action="{{ route('signin') }}" method="post">
                <div class="form-group">
                    <label for="email">Your Email</label>
                    <input class="form-control" type="text" name="email" id="email" value="{{Request::old('email')}}">
                    @if ($errors->has('email'))
                        <span style="color: red">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password">Your Password</label>
                    <input class="form-control" type="password" name="password" id="password">
                    @if ($errors->has('email'))
                        <span style="color: red">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <button class="btn-success btn" type="submit">Login</button>
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>
        </div>
    </div>
   @endsection