<?php

Route::group(['middleware'=>['web']], function(){
    Route::get('/', function () {
        return view('welcome');
    });

    Route::post('/signup', [
        'uses'=>'UserController@postSignUp',
        'as'=>'signup',
    ]);

    Route::post('/signin', [
        'uses'=>'UserController@postSignIn',
        'as'=>'signin',
    ]);

    Route::get('/dashboard', [
        'uses'=>'PostController@getDashboard',
        'as'=>'dashboard',
    ]);

    Route::post('/createpost', [
        'uses'=>'PostController@postCreatePost',
        'as'=>'post.create'
    ]);

    Route::get('/post-delete/{post_id}', [
        'uses'=>'PostController@getDeletePost',
        'as'=>'post.delete',
    ]);

});